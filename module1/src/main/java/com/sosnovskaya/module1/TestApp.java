package com.sosnovskaya.module1;

import com.sosnovskaya.module2.external.ExternalClass;

public class TestApp {

    public static void main(String[] args) {
        System.out.println("it's module1");
        ExternalClass externalClass = new ExternalClass();
        System.out.println(externalClass.callExternal());
        System.out.println(externalClass.callBoth());

        //InternalClass internalClass - compilation error
    }
}
