package com.sosnovskaya.module2.external;

import com.sosnovskaya.module2.InternalClass;

public class ExternalClass {

    public String callExternal() {
        return "it's external string";
    }

    public String callBoth() {
        return new InternalClass().callInternal() + " and external";
    }
}
